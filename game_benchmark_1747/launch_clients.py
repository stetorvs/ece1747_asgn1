import subprocess
import sys

if __name__ == '__main__':

	num_clients_to_launch = 500

	if len(sys.argv) == 2:
		num_clients_to_launch = int(sys.argv[1])

	print 'Launching', num_clients_to_launch, 'clients'
	clients = []
	limbo = open('/dev/null')
	for i in range(num_clients_to_launch):
		p = subprocess.Popen(['./client', 'localhost:8080'], stdout=limbo, stderr=limbo)
		clients.append(p)

	for c in clients:
		c.wait()