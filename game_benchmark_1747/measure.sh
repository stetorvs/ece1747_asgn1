#!/bin/sh

time=180
clients=1024

# Not sure why, but this only works for static???
# Oh well, knowing the unbuffer command is still useful

unbuffer ./server config_static.ini 8080 > static.log &
sleep 1
python launch_clients.py $clients &
sleep $time && pkill python && sleep 1 && pkill server

unbuffer ./server config_spread.ini 8080 > spread.log &
sleep 1
python launch_clients.py $clients &
sleep $time && pkill python && sleep 1 && pkill server

unbuffer ./server config_lightest.ini 8080 > lightest.log &
sleep 1
python launch_clients.py $clients &
sleep $time && pkill python && sleep 1 && pkill server
